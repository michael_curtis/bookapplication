package com.journaldev.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */
@Entity
@Table(name="book")
public class Book {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="authors")
	private String authors;
	
	private String title;
	
	@Column(name="price")
	private String price;
	
	@Column(name="overview")
	private String overview;
	
	@Column(name="quantity_available")
	private String quantity_available;
	
	@Column(name="publish_date")
	private String publish_date;
	
	@Column(name="datetime_added")
	private String datetime_added;
	
	@Column(name="image_url")
	private String image_url;
	
	@Column(name="isbn")
	private String isbn;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getPrice(){
		return price;
	}
	
	public void setPrice(String price){
		this.price = price;
	}
	
	
	
	//getter and setter for the data columns
	public String getOverview(){
		return overview;
	}
	
	public void setOverview(String overview){
		this.overview = overview;
	}
	
	public String getQuantity_available(){
		return quantity_available;
	}
	
	public void setQuantity_available(String quantity_available){
		this.quantity_available = quantity_available;
	}
	
	public String getPublish_date(){
		return publish_date;
	}
	
	public void setPublish_date(String publish_date){
		this.publish_date = publish_date;
	}
	
	public String getDatetime_added(){
		return datetime_added;
	}
	
	public void setDatetime_added(String datetime_added){
		this.datetime_added = datetime_added;
	}
	
	public String getIsbn(){
		return isbn;
	}
	
	public void setIsbn(String isbn){
		this.isbn = isbn;
	}
	
	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	@Override
	public String toString(){
		return "id="+id+",authors="+authors+",isbn="+isbn+"  , title="+title;
	}
	
}
