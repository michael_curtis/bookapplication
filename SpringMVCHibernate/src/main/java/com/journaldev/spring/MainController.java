package com.journaldev.spring;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
//	@RequestMapping(value = "/")
//	public String root() {
//		return "home";
//	}
	
    @RequestMapping("/thanks")
    public String thanks () {
	return "thanks";
}
}
