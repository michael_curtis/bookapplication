package com.journaldev.spring;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.journaldev.spring.model.Book;
import com.journaldev.spring.model.User;
import com.journaldev.spring.service.BookService;

@Controller
public class BookController {
	
private BookService bookService;
	
	@Autowired(required=true)
	@Qualifier(value="bookService")
	public void setBookService(BookService ps){
		this.bookService = ps;
	}

//	@RequestMapping(value = "/books", method = RequestMethod.GET)
//	public String listBooks(Model model) {
//		model.addAttribute("book", new Book());
//		model.addAttribute("listBooks", this.bookService.listBooks());
//		return "admin";
//	}
	
	//For add and update user both
	@RequestMapping(value= "/admin", method = RequestMethod.GET)
	public String admin(Model model, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			if (user.getAdminPermissions()) {
				model.addAttribute("book", new Book());
				model.addAttribute("listBooks", this.bookService.listBooks());
				return "admin";
			}
			else
				return "redirect:/";
		} else {
			return "redirect:/?logout=true";
		}
	}
	
	//For add and update book both
	@RequestMapping(value= "/book/add", method = RequestMethod.POST)
	public String addBook(@ModelAttribute("book") Book b, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			if (user.getAdminPermissions()) {
		
				if(b.getId() == 0) {
					//new book, add it
					this.bookService.addBook(b);
				}else{
					//existing book, call update
					this.bookService.updateBook(b);
				}
				
				return "redirect:/admin";
			}
		}
		
		return "redirect:/admin";
	}
	
	@RequestMapping("/book/remove/{id}")
    public String removeBook(@PathVariable("id") int id){
        this.bookService.removeBook(id);
        return "redirect:/admin";
    }
 
    @RequestMapping("/book/edit/{id}")
    public String editBook(@PathVariable("id") int id, Model model){
        model.addAttribute("book", this.bookService.getBookById(id));
        model.addAttribute("listBooks", this.bookService.listBooks());
        return "admin";
    }
    
    
    

}
