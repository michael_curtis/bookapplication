package com.journaldev.spring;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.journaldev.spring.model.User;
import com.journaldev.spring.service.UserService;

@Controller
public class UserController {
	
	private UserService userService;
	
	@Autowired(required=true)
	@Qualifier(value="userService")
	public void setUserService(UserService us){
		this.userService = us;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(@ModelAttribute("user") User u) {
		return "login";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		model.addAttribute("user", new User());
		return "signup";
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String listUsers(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("listUsers", this.userService.listUsers());
		return "user";
	}
	
	//For add and update user both
	@RequestMapping(value= "/user/add", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") User u){
		
		if(!userService.userExistsByEmail(u.getEmail())){
			//new user, add it
			this.userService.addUser(u);
		}else{
			//this.userService.updateUser(u);
		}
		return "redirect:/users";
	}
	
	//For add and update user both
	@RequestMapping(value= "/user/login", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute("user") User u, HttpServletRequest request){
		User user = userService.login(u.getEmail(), u.getPassword());
		if (user != null) {
			request.getSession().setAttribute("user", user);
			
			if (user.getAdminPermissions())
				return "redirect:/admin";
			else	
				return "redirect:/";
		} else {
			return "redirect:/login?usernotfound=true";
		}
	}

	@RequestMapping(value = "/user/signup", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute("user") User u){
		if (!userService.userExistsByEmail(u.getEmail())){
			this.userService.addUser(u);
			return "redirect:/login?created=success";
		}
		return "redirect:/signup?emailexists=true";
	}

	@RequestMapping(value = "/user/logout", method = RequestMethod.GET)
	public String logoutUser(HttpServletRequest request) {
		request.getSession().removeAttribute("user");
		request.getSession().invalidate();
		String referer = request.getHeader("Referer");
		return "redirect:" + referer + "?logout=true";
	}
	
	@RequestMapping("/user/remove/{id}")
    public String removeUser(@PathVariable("id") int id) {
        this.userService.removeUser(id);
        return "redirect:/users";
    }
 
    @RequestMapping(value = "/user/edit/{id}", method = RequestMethod.GET)
    public String editUser(@PathVariable("id") int id, Model model){
        model.addAttribute("user", this.userService.getUserById(id));
        model.addAttribute("listUsers", this.userService.listUsers());
        return "user";
    }
    
    @RequestMapping("/checkout")
    public String register () {
    	return "checkout";
    }


}
