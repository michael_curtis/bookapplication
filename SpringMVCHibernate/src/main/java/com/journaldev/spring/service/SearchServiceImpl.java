package com.journaldev.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.journaldev.spring.dao.SearchDAO;
import com.journaldev.spring.model.Book;

@Service
public class SearchServiceImpl implements SearchService{
	private SearchDAO searchDAO;
	private List<Book> myList = new ArrayList<Book>();

	public SearchDAO getSearchDAO() {
		return searchDAO;
	}

	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	@Override
	@Transactional
	public List<Book> getByIsbn(String isbn) {
		// TODO Auto-generated method stub
		return myList;
	}

	@Override
	@Transactional
	public List<Book> getByAuthor(String author) {
		// TODO Auto-generated method stub
		return myList;
	}

	@Override
	@Transactional
	public List<Book> getByFormat(String author) {
		// TODO Auto-generated method stub
		return myList;
	}

	@Override
	@Transactional
	public List<Book> getByTitle(String title) {
		
		return searchDAO.getByTitle(title);
	}

	@Override
	@Transactional
	public List<Book> getAll() {
		// TODO Auto-generated method stub
		return searchDAO.getAll();
	}
	@Override
	@Transactional
	public List<Book> getByPrice(int low, int high) {
		// TODO Auto-generated method stub
		return myList;
	}

}
