package com.journaldev.spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.journaldev.spring.dao.TransactionDAO;
import com.journaldev.spring.model.Transaction;

@Service
public class TransactionServiceImpl implements TransactionService{
	
	private TransactionDAO transactionDAO;
	
	
	public void setTransactionDAO(TransactionDAO transactionDAO){
		this.transactionDAO = transactionDAO;
	}
	
	@Override
	@Transactional
	public void addTransaction(Transaction t) {
		this.transactionDAO.addTransaction(t);
	}

	@Override
	@Transactional
	public void updateTransaction(Transaction t) {
		this.transactionDAO.updateTransaction(t);
	}

	@Override
	@Transactional
	public List<Transaction> listTransactions() {
		return this.transactionDAO.listTransactions();
	}

	@Override
	@Transactional
	public Transaction getTransactionById(int id) {
		return this.transactionDAO.getTransactionById(id);
	}

	@Override
	@Transactional
	public void removeTransaction(int id) {
		this.transactionDAO.removeTransaction(id);
		
	}

}
