package com.journaldev.spring.service;

import java.util.List;

import com.journaldev.spring.model.Book;

public interface BookService {

	
	public void addBook(Book b);
	public void updateBook(Book b);
	public List<Book> listBooks();
	public Book getBookById(int id);
	
	public Book getBookByPrice(int price);
	public Book getBookByPublish_date(int publish_date);
	public Book getBookByDatetime_added(int datetime_added);
	public Book getBookByQuantity_available(int quantity_available);
	public Book getBookByOverview(String overview);
	public Book getBookByImage_url(String image_url);
	
	public Book getBookByIsbn(String isbn);
	public void removeBook(int id);
	
}
