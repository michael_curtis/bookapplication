package com.journaldev.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.BookDAO;
import com.journaldev.spring.model.Book;


@Service
public class BookServiceImpl implements BookService {
	
	
	private BookDAO bookDAO;
	

	public void setBookDAO(BookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}

	@Override
	@Transactional
	public void addBook(Book b) {
		this.bookDAO.addBook(b);
	}

	@Override
	@Transactional
	public void updateBook(Book b) {
		this.bookDAO.updateBook(b);
	}

	@Override
	@Transactional
	public List<Book> listBooks() {
		return this.bookDAO.listBooks();
	}

	@Override
	@Transactional
	public Book getBookById(int id) {
		return this.bookDAO.getBookById(id);
	}
	
	@Override
	@Transactional
	public Book getBookByPrice(int price) {
		return this.bookDAO.getBookByPrice(price);
	}
	
	@Override
	@Transactional
	public Book getBookByQuantity_available(int quantity_available) {
		return this.bookDAO.getBookByQuantity_available(quantity_available);
	}
	
	@Override
	@Transactional
	public Book getBookByOverview(String overview) {
		return this.bookDAO.getBookByOverview(overview);
	}
	
	@Override
	@Transactional
	public Book getBookByDatetime_added(int datetime_added) {
		return this.bookDAO.getBookByDatetime_added(datetime_added);
	}
	
	@Override
	@Transactional
	public Book getBookByPublish_date(int publish_date) {
		return this.bookDAO.getBookByPublish_date(publish_date);
	}

	@Override
	@Transactional
	public Book getBookByIsbn(String isbn) {
		return this.bookDAO.getBookByIsbn(isbn);
	}
	
	@Override
	@Transactional
	public void removeBook(int id) {
		this.bookDAO.removeBook(id);
	}

	@Override
	public Book getBookByImage_url(String image_url) {
		// TODO Auto-generated method stub
		return this.bookDAO.getBookByImage_url(image_url);
	}


}
