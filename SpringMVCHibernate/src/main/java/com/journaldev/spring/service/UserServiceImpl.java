package com.journaldev.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.UserDAO;
import com.journaldev.spring.model.User;

@Service
public class UserServiceImpl implements UserService {
	
	private UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	@Transactional
	public void addUser(User u) {
		this.userDAO.addUser(u);
	}

	@Override
	@Transactional
	public void updateUser(User u) {
		this.userDAO.updateUser(u);
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return this.userDAO.listUsers();
	}
	
	@Override
	@Transactional
	public User getUserById(int id){
		return this.userDAO.getUserById(id);
	}

	@Override
	@Transactional
	public User getUserByEmail(String email) {
		return this.userDAO.getUserByEmail(email);
	}

	@Override
	@Transactional
	public void removeUser(int id) {
		this.userDAO.removeUser(id);
	}
	
	@Override
	@Transactional
	public boolean userExistsByEmail(String email){
		return this.userDAO.userExistsByEmail(email);
	}
	
	@Override
	@Transactional
	public boolean userExistsById(int id){
		return this.userDAO.userExistsById(id);
	}

	@Override
	@Transactional
	public User login(String email, String password) {
		return this.userDAO.login(email,password);
	}

}
