package com.journaldev.spring.service;
import com.journaldev.spring.dao.SearchDAO;
import com.journaldev.spring.model.Book;

import java.util.List;
public interface SearchService {

	public List<Book> getByIsbn(String isbn);
	public List<Book> getByAuthor(String author);
	public List<Book> getByFormat(String author);
	public List<Book> getByTitle(String title);
	public List<Book> getByPrice(int low, int high);
	public SearchDAO getSearchDAO();
	public void setSearchDAO(SearchDAO searchDAO);
	public List<Book> getAll();
}
