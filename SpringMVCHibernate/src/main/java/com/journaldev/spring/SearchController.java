package com.journaldev.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.journaldev.spring.model.Book;


import com.journaldev.spring.service.SearchService;

@Controller
public class SearchController {
	private SearchService searchService;
	
	@Autowired(required=true)
	@Qualifier(value="searchService")
	public void setSearchService(SearchService ss){
		this.searchService = ss;
	}
	@SuppressWarnings("unused")
	@RequestMapping(value = "/")
	public String root(@ModelAttribute("book") Book book, Model model) {
		List<Book> myList = this.searchService.getAll();
		model.addAttribute("bookList", myList);
		return "home";
	}

	@RequestMapping(value = "/search")
	public String searchForBook(@ModelAttribute("book") Book book, Model model) {
		List<Book> myList = this.searchService.getByTitle(book.getTitle());
		model.addAttribute("bookList", myList);
		return "ContinueShopping";
	}
    @RequestMapping("/ContinueShopping")
    public String continueShopping (Model model) {
		model.addAttribute("book", new Book());
		List<Book> myList = this.searchService.getAll();
		model.addAttribute("bookList", myList);
    	return "ContinueShopping";
    }
	
}
