package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.journaldev.spring.model.Book;

public class SearchDAOImpl implements SearchDAO {
	private SessionFactory sessionFactory;
	private static final Logger logger = LoggerFactory.getLogger(SearchDAOImpl.class);
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Book> getAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Book").list();
	}

	@Override
	public Book getByID(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getByTitle(String title) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Book WHERE title LIKE '%" + title + "%'").list();
	}

	@Override
	public List<Book> getByIsbn(int isbn) {
		// TODO Auto-generated method stub
		return null;
	}
}
