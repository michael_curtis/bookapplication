package com.journaldev.spring.dao;

import java.util.List;

import com.journaldev.spring.model.Transaction;

public interface TransactionDAO {
	public void addTransaction(Transaction t);
	public void updateTransaction(Transaction t);
	public List<Transaction> listTransactions();
	public Transaction getTransactionById(int id);
	public void removeTransaction(int id);
	
}
