package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.internal.EntityManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.journaldev.spring.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addUser(User u) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(u);
		logger.info("User saved successfully, User Details="+u);
	}

	@Override
	public void updateUser(User u) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(u);
		logger.info("User updated successfully, User Details="+u);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
		Session session = this.sessionFactory.getCurrentSession();
		List<User> usersList = session.createQuery("from User").list();
		for(User u : usersList){
			logger.info("User List::"+u);
		}
		return usersList;
	}

	@Override
	public User getUserById(int id){
		Session session = this.sessionFactory.getCurrentSession();
		User u = (User) session.load(User.class, new Integer(id));
		logger.info("User loaded successfully, User details=" + u);
		return u;
	}
	
	@Override
	public User getUserByEmail(String email) {
		Session session = this.sessionFactory.getCurrentSession();		
		User u = (User) session.load(User.class, new String(email));
		logger.info("User loaded successfully, User details="+u);
		return u;
	}

	@Override
	public void removeUser(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		User u = (User) session.load(User.class, new Integer(id));
		if(null != u){
			session.delete(u);
		}
		logger.info("User deleted successfully, user details="+u);
	}
	
	@Override
	public boolean userExistsById(int id){
		Session session = this.sessionFactory.getCurrentSession();
		User u = null;
		
		try {
			u = (User) session.get(User.class, id);
		} catch (HibernateException e) {
			return false;
		}
		
		return (u != null);
	}

	@Override
	public boolean userExistsByEmail(String email) {
		Session session = this.sessionFactory.getCurrentSession();
		Long count = (Long) session.createQuery("Select count(*) from User u where u.email = :email")
				.setString("email", email)
				.uniqueResult();
		boolean exists = count > 0;
		return exists;
		
        /**
		Session session = this.sessionFactory.getCurrentSession();
        User u = null;
        
        try {
        	u = (User) session.get(User.class, email);
        } catch (HibernateException e) {
        	return false;
        }
        
        return (u != null);
        */
    }

	@Override
	public User login(String email, String password) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.createQuery("from User u where u.email = :email and u.password = :password")
				.setString("email", email)
				.setString("password", password)
				.uniqueResult();
		return user;		
	}
    

}
