package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.journaldev.spring.model.Book;
import com.journaldev.spring.model.User;

public class BookDAOImpl implements BookDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	
	@Override
	public void addBook(Book b) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(b);
		logger.info("Book saved successfully, Book Details="+b);
	}

	@Override
	public void updateBook(Book b) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(b);
		logger.info("Book updated successfully, Book Details="+b);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> listBooks() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Book> booksList = session.createQuery("from Book").list();
		for(Book b : booksList){
			logger.info("Book List::"+b);
		}
		return booksList;
	}

	@Override
	public Book getBookById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Book b = (Book) session.load(Book.class, new Integer(id));
		logger.info("Book loaded successfully, Book details="+b);
		return b;
	}
	
	@Override
	public Book getBookByIsbn(String isbn) {
		Session session = this.sessionFactory.getCurrentSession();		
		Book b = (Book) session.load(Book.class, new String(isbn));
		logger.info("User loaded successfully, User details="+b);
		return b;
	}
	

	@Override
	public void removeBook(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Book b = (Book) session.load(Book.class, new Integer(id));
		if(null != b){
			session.delete(b);
		}
		logger.info("Book deleted successfully, book details="+b);
	}


	@Override
	public Book getBookByPrice(int price) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Book getBookByPublish_date(int publish_date) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Book getBookByDatetime_added(int datetime_added) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Book getBookByQuantity_available(int quantity_available) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Book getBookByOverview(String overview) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Book getBookByImage_url(String image_url) {
		// TODO Auto-generated method stub
		return null;
	}

}
