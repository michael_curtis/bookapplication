package com.journaldev.spring.dao;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;

import com.journaldev.spring.model.Transaction;

@Repository
public class TransactionDAOImpl implements TransactionDAO{

	private static final Logger logger = LoggerFactory.getLogger(TransactionDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addTransaction(Transaction t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(t);
		logger.info("Transaction saved successfully, Transaction Details=" + t);
	}

	@Override
	public void updateTransaction(Transaction t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(t);
		logger.info("Transaction updated successfully, Transaction Details=" + t);	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> listTransactions() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Transaction> transactionsList = session.createQuery("from Transaction").list();
		for (Transaction t : transactionsList){
			logger.info("Transaction List::" + t);
		}
		return transactionsList;
	}

	@Override
	public Transaction getTransactionById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Transaction t = (Transaction) session.load(Transaction.class, new Integer(id));
		logger.info("Transaction loaded successfully, Transaction details=" + t);
		return t;
	}

	@Override
	public void removeTransaction(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Transaction t = (Transaction) session.load(Transaction.class, new Integer(id));
		if (null != t){
			session.delete(t);
		}
		logger.info("Transaction deleted successfully, transaction details=" + t);
	}

}
