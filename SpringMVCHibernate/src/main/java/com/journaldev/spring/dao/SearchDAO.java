package com.journaldev.spring.dao;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.model.Book;
public interface SearchDAO {
	public List<Book> getAll();
	public Book getByID(int id);
	public List<Book> getByTitle(String title);
	public List<Book> getByIsbn(int isbn);
}
