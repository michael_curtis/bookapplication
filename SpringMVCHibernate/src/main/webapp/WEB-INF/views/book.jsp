<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>
<html>
<head>
	<title>Book | Bookworm</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Book
</h1>

<c:url var="addAction" value="/book/add" ></c:url>



<form:form action="${addAction}" commandName="book">
<table>
	<c:if test="${!empty book.authors}">
	<tr>
		<td>
			<form:label path="id">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="id" readonly="true" size="8"  disabled="true" />
			<form:hidden path="id" />
		</td> 
	</tr>
	</c:if>
	
	<tr>
		<td>
			<form:label path="isbn">
				<spring:message text="ISBN"/>
			</form:label>
		</td>
		<td>
			<form:input path="isbn"  />

		</td> 
	</tr>

 	<tr>
		<td>
			<form:label path="authors">
				<spring:message text="Authors"/>
			</form:label>
		</td>
		<td>
			<form:input path="authors" />
		</td> 
	</tr>
	
	
	<tr>
		<td>
			<form:label path="title">
				<spring:message text="Title"/>
			</form:label>
		</td>
		<td>
			<form:input path="title" />
		</td>
	</tr>
	
	<tr>
		<td>
			<form:label path="price">
				<spring:message text="Price"/>
			</form:label>
		</td>
		<td>
			<form:input path="price" />
		</td>
	</tr>
	
	
	<tr>
		<td>
			<form:label path="quantity_available">
				<spring:message text="Quantity"/>
			</form:label>
		</td>
		<td>
			<form:input path="quantity_available" />
		</td>
	</tr>
	
	<tr>
		<td>
			<form:label path="publish_date">
				<spring:message text="Published Date"/>
			</form:label>
		</td>
		<td>
			<form:input path="publish_date" />
		</td>
	</tr>
	
	<tr>
		<td>
			<form:label path="datetime_added">
				<spring:message text="Added Date"/>
			</form:label>
		</td>
		<td>
			<form:input path="datetime_added" />
		</td>
	</tr>
	
	<tr>
		<td>
			<form:label path="overview">
				<spring:message text="Overview"/>
			</form:label>
		</td>
		<td>
			<form:input path="overview" />
		</td>
	</tr>
	
	<tr>
		<td>
			<form:label path="image_url">
				<spring:message text="Image_url"/>
			</form:label>
		</td>
		<td>
			<form:input path="image_url" />
		</td>
	</tr>

	
	<tr>
		<td colspan="2">
			<c:if test="${!empty book.authors}">
				<input type="submit"
					value="<spring:message text="Edit Book"/>" />
			</c:if>
			<c:if test="${empty book.authors}">
				<input type="submit"
					value="<spring:message text="Add Book"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>Books List</h3>
<c:if test="${!empty listBooks}">
	<table class="tg">
	<tr>
		<th width="60">Book ID</th>
		<th width="120">Book Authors</th>
		<th width="120">Book Title</th>
		<th width="80">ISBN</th>
		
		<th width="80">Price</th>
		<th width="80">Quantity </th>
		<th width="120">Image </th>
		
		<th width="60">Edit</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listBooks}" var="book">
		<tr>
			<td>${book.id}</td>
			<td>${book.authors}</td>
			<td>${book.title}</td>
			<td>${book.isbn}</td>
			<td>${book.price}</td>
			<td>${book.quantity_available}</td>
			<td><img SRC="${book.image_url}" width=50 height=50></td>
			
			
			<td><a href="<c:url value='/book/edit/${book.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/book/remove/${book.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
</body>
</html>
