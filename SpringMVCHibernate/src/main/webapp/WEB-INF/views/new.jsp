<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Owl Carousel - Images Demo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="jQuery Responsive Carousel - Owl Carusel">
    <meta name="author" content="Bartosz Wojciechowski">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="resources/Carousel/assets/css/bootstrapTheme.css" rel="stylesheet">
    <link href="resources/Carousel/assets/css/custom.css" rel="stylesheet">

    <!-- Owl Carousel Assets -->
    <link href="resources/Carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="resources/Carousel/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- Prettify -->
    <link href="resources/Carousel/assets/js/google-code-prettify/prettify.css" rel="stylesheet">
    

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="resources/Carousel/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/Carousel/assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="resources/Carousel/assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="resources/Carousel/assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="resources/Carousel/assets/ico/favicon.png">
    <style>
    	.item>img {
    		/*max-width: 90%;*/
    		max-height: 450px;
    		margin: auto;
    		width: auto;
    		height: auto;
    		}
    </style>
  </head>
  <body>

              <div id="owl-demo" class="owl-carousel">
  <div class="item"><img src="resources/Carousel/demos/GeorgeWashington.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/TheLostIsland.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/IWasHere.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/JesusCalling.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/SevenEves.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/TheCellar.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/TheAlchemist.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/TheWitnessoftheStars.jpeg" alt="Owl Image"></div>
  <div class="item"><img src="resources/Carousel/demos/TheHarbringer.jpeg" alt="Owl Image"></div>
              </div>
              
            </div>
          </div>
        </div>

    </div>


            </div><!--End Tab Content-->

          </div>
        </div>
      </div>
    </div>


    <script src="resources/Carousel/assets/js/jquery-1.9.1.min.js"></script> 
    <script src="resources/Carousel/owl-carousel/owl.carousel.js"></script>


    <!-- Demo -->

    <style>
    #owl-demo .item{
        margin: 3px;
    }
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
    }
    </style>


    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
      });

    });
    </script>

        <script src="resources/Carousel/assets/js/bootstrap-collapse.js"></script>
        <script src="resources/Carousel/assets/js/bootstrap-transition.js"></script>
        <script src="resources/Carousel/assets/js/bootstrap-tab.js"></script>
        <script src="resources/Carousel/assets/js/google-code-prettify/prettify.js"></script>
	    <script src="resources/Carousel/assets/js/application.js"></script>

  </body>
</html>
