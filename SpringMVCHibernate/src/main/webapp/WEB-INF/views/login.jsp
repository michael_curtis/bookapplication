<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>
<html>

<head>
    <link rel="stylesheet" href="resources/css/ourStyle.css">
    <link rel="stylesheet" href="resources/css/foundation.min.css">
    <link rel="stylesheet" href="resources/css/overwriteFoundationStyles.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log in | Bookworm</title>
</head>

<body class="bckImg">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" ></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<nav>
</nav>

<div class="center-form">
    <div class="text-center" style="padding-bottom:30px;">
   		<h3 class="text-center" style="color:#213963; padding-bottom:20px;">Sign In</h3>
        <a href="/Bookworm"><img src="resources/BookWormLogo/BookWormLogoCir3.png" style="width:128px" alt="Book Picture" /></a>
    </div>
    
	<form:form action="user/login" method="POST" modelAttribute="user">
		
		<c:if test="${not empty param.usernotfound}">
			<p style="color:red"><c:out value = "The email address and password you provided don't match." /></p>
		</c:if>
		
		<c:if test="${not empty param.created}">
			<script type="text/javascript">toastr.success('Thank you for registering!', 'Account created!', {timeout:2000});</script>
		</c:if>
		
		<form:label path="email" style="color: #213963">
			<spring:message text="Email"/>
		</form:label>
		<form:input path="email" required="true" autofocus="true" type="email" />
		
		<form:label path="password" style="color: #213963">
			<spring:message text="Password"/>
		</form:label>
		<form:input path="password" type="password" required="true"/>
		
		<input type="submit" class="button medium float-center" style="background-color:#213963"  value="<spring:message text="Sign In"/>" />
	</form:form>        

    
    <p style="font-size:medium" style="color: #213963">New user? <a href="signup">Sign up now.</a></p>
</div>

</body>
</html>