<%@ page session="true"%>
<c:if test="${!ajaxRequest}">
	<html>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700'
	rel='stylesheet' type='text/css'>
<link href="resources/Carousel/assets/css/bootstrapTheme.css"
	rel="stylesheet">
<link href="resources/Carousel/assets/css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="resources/css/ourStyle.css">
<link rel="stylesheet" href="resources/css/foundation.min.css">
<link rel="stylesheet"
	href="resources/css/overwriteFoundationStyles.css">
<link rel="stylesheet" href="resources/shopping-cart/css/normalize.css">
<link rel="stylesheet" href="resources/shopping-cart/css/style.css">
<script src="resources/js/jquery-2.1.1.js"></script>

<head>
<title>Thanks! | Bookworm</title>
<br>
	<div class="row">
		<a href="/Bookworm"><img
			src="resources/BookWormLogo/BookWormLogoLong.png"
			height="230" width="230" align="left"
			style="padding-left: 10px; padding-right: 20px" alt=""></a>
			</div>
			<title>Thanks | Bookworm</title>
</head>
</c:if>


				<div class="large-12 columns">
					


<div class="row"><hr></div>
<div class="main">

	
	<center><a href="/Bookworm">Return To Home</a></center>
	<br>
	
<hr>

<br>

<h1 style="font-size: 200%">Thank You For Your Purchase!</h1>



<script
	src='http://cdnjs.cloudflare.com/ajax/libs/zepto/1.0/zepto.min.js'></script>
</div>


<script src="resources/js/jquery.mixitup.min.js"></script>
<script src="resources/js/main.js"></script>
<!-- Resource jQuery -->


<script src="resources/js/vendor/foundation.js"></script>
<!-- Other JS plugins can be included here -->

<script>
	$(document).foundation();
</script>
</div>
</div>
<c:if test="${!ajaxRequest}">
	</body>
	</html>
</c:if>