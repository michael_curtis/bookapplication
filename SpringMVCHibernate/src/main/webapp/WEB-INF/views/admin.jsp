<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<html>
<link rel="stylesheet" href="/Bookworm/resources/css/ourStyle.css">
<link rel="stylesheet" href="/Bookworm/resources/css/foundation.min.css">
<link rel="stylesheet" href="/Bookworm/resources/css/overwriteFoundationStyles.css">
<head>
	<title>Admin | Bookworm</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Calibri, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Calibri, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<jsp:include page="header.jsp"/>

<body style="background-color: #ecf0f1">
	<hr>
	<div class="row">
		<div class="columns medium-4" style="background-color: #dde4e6; border-radius:25px; padding:15px; margin-left:25px">
			<h1
				style="font: Calibri; color: #213963; font-size: 36px; text-align: center">
				Add a Book</h1>
			<form:form action="/Bookworm/book/add" commandName="book">
				<c:if test="${!empty book.authors}">
					<form:label path="id">
						<spring:message text="ID" />
					</form:label>
					<form:input path="id" readonly="true" size="8" disabled="true" />
					<form:hidden path="id" />
				</c:if>
				<form:label path="isbn">
					<spring:message text="ISBN" />
				</form:label>
				<form:input path="isbn" required="true"/>

				<form:label path="authors">
					<spring:message text="Authors" />
				</form:label>
				<form:input path="authors" required="true"/>

				<form:label path="title">
					<spring:message text="Title" />
				</form:label>

				<form:input path="title" required="true"/>

				<form:label path="price">
					<spring:message text="Price" />
				</form:label>

				<form:input path="price" required="true"/>

				<form:label path="quantity_available">
					<spring:message text="Quantity" />
				</form:label>

				<form:input path="quantity_available" type="number" required="true" />

				<form:label path="publish_date">
					<spring:message text="Published Date" />
				</form:label>

				<form:input path="publish_date" type="date" required="true"/>

				<form:label path="datetime_added">
					<spring:message text="Added Date" />
				</form:label>

				<form:input path="datetime_added" type="date" required="true"/>

				<form:label path="overview">
					<spring:message text="Overview" />
				</form:label>

				<form:input path="overview" required="true"/>

				<form:label path="image_url">
					<spring:message text="Image URL" />
				</form:label>

				<form:input path="image_url" required="true"/>

				<c:if test="${!empty book.authors}">
					<input type="submit" style="background-color: #213963" class="button float-center" value='<spring:message text="Edit Book"/>' />
				</c:if>
				<c:if test="${empty book.authors}">
					<input type="submit" class="button float-center"
						style="background-color: #213963"
						value='<spring:message text="Add Book"/>' />
				</c:if>
				<table></table>
			</form:form>
		</div>
		<div class="columns medium-1"></div>
		<div class="columns medium-7">
			<h1
				style="font: Calibri; color: #213963; font-size: 36px; text-align: center">Books
				List</h1>
			<c:if test="${!empty listBooks}">
				<table class="tg">
					<tr>
						<th width="60">Book ID</th>
						<th width="120">Book Authors</th>
						<th width="120">Book Title</th>
						<th width="80">ISBN</th>

						<th width="80">Price</th>
						<th width="80">Quantity</th>

						<th width="120">Overview</th>
						<th width="120">Image</th>

						<th width="60">Edit</th>
						<th width="60">Delete</th>
					</tr>
					<c:forEach items="${listBooks}" var="book">
						<tr>
							<td>${book.id}</td>
							<td>${book.authors}</td>
							<td>${book.title}</td>
							<td>${book.isbn}</td>
							<td>${book.price}</td>
							<td>${book.quantity_available}</td>
							<td class="long">${book.overview}</td>
							<td><img SRC="${book.image_url}" width="50" height="50"
								alt=""></td>

							<td><a href="<c:url value='/book/edit/${book.id}' />" >Edit</a></td>
							<td><a href="<c:url value='/book/remove/${book.id}' />" >Delete</a></td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</div>
</body>
<script src="/Bookworm/resources/js/jquery-2.1.1.js" type="text/javascript"></script>
<script src="/Bookworm/resources/js/vendor/foundation.js" type="text/javascript"></script>
<script type="text/javascript">
	$("td.long").each(function() {
		if (this.innerHTML.length > 46) {
			this.innerHTML = this.innerHTML.substring(0, 45) + "...";
		}
	})
</script>

</html>
