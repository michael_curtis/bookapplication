<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>
<html>
<head>
	<title>Transactions | Bookworm</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Transaction
</h1>

<c:url var="addAction" value="/transaction/add" ></c:url>

<form:form action="${addAction}" commandName="transaction">
<table>
	<c:if test="${!empty transaction.id}">
	<tr>
		<td>
			<form:label path="id">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="id" readonly="true" size="8"  disabled="true" />
			<form:hidden path="id" />
		</td> 
	</tr>
	</c:if>
	<tr> 
		<td>
			<form:label path="totalCost">
				<spring:message text="Total Cost"/>
			</form:label>
		</td>
		<td>
			<form:input path="totalCost" />
		</td> 
	</tr>
	<tr>
		<td>
			<form:label path="isActive">
				<spring:message text="Is Active"/>
			</form:label>
		</td>
		<td>
			<form:input path="isActive" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
				<input type="submit"
					value="<spring:message text="Add Transaction"/>" />
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>Transaction List</h3>
<c:if test="${!empty listTransactions}">
	<table class="tg">
	<tr>
		<th width="120">Transaction ID</th>
		<th width="120">Total Cost</th>
		<th width="120">Is Active</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listTransactions}" var="transaction">
		<tr>
			<td>${transaction.id}</td>
			<td>${transaction.totalCost}</td>
			<td>${transaction.isActive}</td>
			<td><a href="<c:url value='transaction/remove/${transaction.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
</body>
</html>
