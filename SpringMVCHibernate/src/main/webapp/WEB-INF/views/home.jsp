<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<html>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700'
	rel='stylesheet' type='text/css'>
<link href="resources/Carousel/assets/css/bootstrapTheme.css"
	rel="stylesheet">
<link href="resources/Carousel/assets/css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="resources/css/ourStyle.css">
<link rel="stylesheet" href="resources/css/foundation.min.css">
<link rel="stylesheet"
	href="resources/css/overwriteFoundationStyles.css">
<link rel="stylesheet" href="resources/shopping-cart/css/normalize.css">
<link rel="stylesheet" href="resources/shopping-cart/css/style.css">
<script src="resources/js/jquery-2.1.1.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" ></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Home | Bookworm</title>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
<meta class="foundation-mq">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
</head>
<body>
	
	<div class="row">
		<c:if test="${not empty param.logout}">
		<script type="text/javascript">toastr.success('Goodbye!', 'Successfully logged out', {timeout:2000});</script>
	</c:if>
		<div class="large-1 columns" style="padding-right: 20px">
		<div id = "filters">
			<div class="cd-filter">
				<form action="">
					<div class="cd-filter-block">
					
						<h4>Search</h4>

						<div class="cd-filter-content">
							<input type="search" placeholder="Title, Author, ISBN#"
								style="font-color: #213963">

							<ul class="cd-filter-content cd-filters list">
								<li><input class="filter" data-filter=".radio1"
									type="radio" name="search" id="radio1" checked> <label
									class="radio-label" for="radio1">Title</label></li>

								<li><input class="filter" data-filter=".radio2"
									type="radio" name="search" id="radio2"> <label
									class="radio-label" for="radio2">Author</label></li>

								<li><input class="filter" data-filter=".radio3"
									type="radio" name="search" id="radio3"> <label
									class="radio-label" for="radio3">ISBN#</label></li>
							</ul>

						</div>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Book Type</h4>

						<ul class="cd-filter-content cd-filters list">
							<li><input class="filter" data-filter=".check1"
								type="checkbox" id="checkbox1"> <label
								class="checkbox-label" for="checkbox1">Print Book</label></li>

							<li><input class="filter" data-filter=".check2"
								type="checkbox" id="checkbox2"> <label
								class="checkbox-label" for="checkbox2">EBook</label></li>

						</ul>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Price Range</h4>

						<div class="cd-filter-content">
							<div class="cd-select cd-filters">
								<select class="filter" name="selectThis" id="selectThis">
									<option value="">Choose an option</option>
									<option value=".option1">$0 - $10</option>
									<option value=".option2">$10 - $50</option>
									<option value=".option3">$50 - $100</option>
									<option value=".option4">Over $100</option>
								</select>
							</div>
							<!-- cd-select -->
						</div>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Genre</h4>

						<ul class="cd-filter-content cd-filters list">
							<li><input class="filter" data-filter=".radio1" type="radio"
								name="genre" id="radio4" checked> <label
								class="radio-label" for="radio4">Search All</label></li>

							<li><input class="filter" data-filter=".radio1" type="radio"
								name="genre" id="radio5"> <label class="radio-label"
								for="radio5">Fiction</label></li>

							<li><input class="filter" data-filter=".radio2" type="radio"
								name="genre" id="radio6"> <label class="radio-label"
								for="radio6">Non-Fiction</label></li>

							<li><input class="filter" data-filter=".radio3" type="radio"
								name="genre" id="radio7"> <label class="radio-label"
								for="radio7">Teen</label></li>

							<li><input class="filter" data-filter=".radio4" type="radio"
								name="genre" id="radio8"> <label class="radio-label"
								for="radio8">Children</label></li>

							<li><input class="filter" data-filter=".radio5" type="radio"
								name="genre" id="radio9"> <label class="radio-label"
								for="radio9">Textbook/Study Help</label></li>

							<li><input class="filter" data-filter=".radio6" type="radio"
								name="genre" id="radio10"> <label class="radio-label"
								for="radio10">Sale</label></li>
						</ul>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->
				</form>
				<a href="#0" class="cd-close">Close</a>
			</div>
	
			<!-- cd-filter -->

			<a href="#0" class="cd-filter-trigger">Refine Search</a>
		</div>
		</div>
		<div id="content" class="large-11s columns" style="padding-left: 40px">
			<jsp:include page="header.jsp" />
			<br>
		</div>


	</div>
	<hr>
	<!--  <h1 style="font-size: 260%">Need a good read? Try one of these!</h1> -->
	<div class="row">
		<body>
			<div class="row">


				<div class="large-12 columns">
					<head>

<title>BookWorm</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="jQuery Responsive Carousel - Owl Carusel">
<meta name="author" content="Bartosz Wojciechowski">
<meta charset="utf-8">


<!-- Owl Carousel Assets -->
<link href="resources/Carousel/owl-carousel/owl.carousel.css"
	rel="stylesheet">
<link href="resources/Carousel/owl-carousel/owl.theme.css"
	rel="stylesheet">

<!-- Prettify -->
<link
	href="resources/Carousel/assets/js/google-code-prettify/prettify.css"
	rel="stylesheet">


<!-- Le fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="resources/Carousel/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="resources/Carousel/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="resources/Carousel/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="resources/Carousel/assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon"
	href="resources/Carousel/assets/ico/favicon.png">
<style type="text/css">
.item>img {
	/*max-width: 90%;*/
	max-height: 250px;
	margin: auto;
	width: 100%;
	height: auto;
}
</style>
</head>
					<body>

						<div style="padding-left: 15px" id="owl-demo" class="owl-carousel">
							<div class="item">
								<img src="resources/Carousel/demos/GeorgeWashington.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/TheLostIsland.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/IWasHere.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/JesusCalling.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/SevenEves.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/TheCellar.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/TheAlchemist.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/TheWitnessoftheStars.jpeg"
									alt="Owl Image">
							</div>
							<div class="item">
								<img src="resources/Carousel/demos/TheHarbringer.jpeg"
									alt="Owl Image">
							</div>
						</div>
					</body>
				</div>
			</div>
		</body>
	</div>
	<script src="resources/Carousel/assets/js/jquery-1.9.1.min.js"
		type="text/javascript"></script>
	<script src="resources/Carousel/owl-carousel/owl.carousel.js"
		type="text/javascript"></script>


	<!-- Demo -->

	<style type="text/css">
#owl-demo .item {
	margin: 3px;
}

#owl-demo .item img {
	display: block;
	width: 50%;
	height: 50%;
}
</style>


	<script type="text/javascript">
		$(document).ready(function() {
			$("#owl-demo").owlCarousel({
				autoPlay : 3000,
				items : 4,
				itemsDesktop : [ 1199, 3 ],
				itemsDesktopSmall : [ 979, 3 ]
			});

		});
	</script>

	<script src="resources/Carousel/assets/js/bootstrap-collapse.js"
		type="text/javascript"></script>
	<script src="resources/Carousel/assets/js/bootstrap-transition.js"
		type="text/javascript"></script>
	<script src="resources/Carousel/assets/js/bootstrap-tab.js"
		type="text/javascript"></script>
	<script
		src="resources/Carousel/assets/js/google-code-prettify/prettify.js"
		type="text/javascript"></script>
	<script src="resources/Carousel/assets/js/application.js"
		type="text/javascript"></script>

</body>

<div class="row">
	<hr>
</div>
<div class="main">
	<br>
	<center>
		<a href="ContinueShopping">Shop All Books and eBooks <img
			src="resources/img/arrow.png" height="10px" width="10px"
			style="margin-Top: -2px" alt=""></img></a>
	</center>
	<br>
	<hr>

	<h1 style="font-size: 200%">
		<b>Featured Book Of The Week</b>
	</h1>
	<br>




	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/zepto/1.0/zepto.min.js'
		type="text/javascript"></script>



	<div class="row">
		<img
			src="https://images-na.ssl-images-amazon.com/images/I/51JBIqZ0fwL._SX331_BO1,204,203,200_.jpg"
			height="230" width="230" align="left"
			style="padding-left: 10px; padding-right: 20px" alt="">
		<h2 style="font-size: 140%">
			<b>I Was Here</b>
		</h2>
		<h2 style="font-size: 80%">
			<b><i>The newest heart-wrenchingly powerful novel from the
					bestselling author of IF I STAY.</i></b>
		</h2>
		<p align="justify">When her best friend Meg drinks a bottle of
			industrial-strength cleaner alone in a motel room, Cody is
			understandably shocked and devastated. She and Meg shared everything,
			so how was there no warning? But when Cody travels to Meg's college
			town to pack up the belongings left behind, she discovers that
			there's a lot that Meg never told her. About her old roommates, the
			sort of people Cody never would have met in her dead-end small town
			in Washington. About Ben McAllister, the boy with a guitar and a
			sneer, and some secrets of his own. And about an encrypted computer
			file that Cody can't open until she does, and suddenly everything
			Cody thought she knew about her best friend's death gets thrown into
			question.</p>
	</div>
	<br>


	<hr>

	<footer>

		<div class="row">
			<div class="small-3 medium-1 columns">
				<p class="bottom">Contact Us:</p>
			</div>

			<div class="small-3 medium-2 columns">
				<p class="bottom">
					<a>Phone (555) 555-5555</a>
				</p>
			</div>

			<div class="small-3 medium-2 columns">
				<a href="mailto:helpdesk@bookworm.com?Subject=Customer Inquiry"
					target="_top" class="bottom">Send Email</a>
			</div>

			<div class="small-3 columns">
				<p class="bottom" align="right">&copy BookWorm 2016</p>
			</div>
		</div>
		<br>

	</footer>




	<script src="resources/js/jquery.mixitup.min.js" type="text/javascript"></script>
	<script src="resources/js/main.js" type="text/javascript"></script>
	<!-- Resource jQuery -->


	<script src="resources/js/vendor/foundation.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).foundation();
	</script>
	<!--</div>-->
</div>
</html>