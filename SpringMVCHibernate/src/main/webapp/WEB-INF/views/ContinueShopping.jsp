<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>
<c:if test="${!ajaxRequest}">
	<html>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700'
	rel='stylesheet' type='text/css'>
<link href="resources/Carousel/assets/css/bootstrapTheme.css"
	rel="stylesheet">
<link href="resources/Carousel/assets/css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="resources/css/ourStyle.css">
<link rel="stylesheet" href="resources/css/foundation.min.css">
<link rel="stylesheet"
	href="resources/css/overwriteFoundationStyles.css">
<link rel="stylesheet" href="resources/shopping-cart/css/normalize.css">
<link rel="stylesheet" href="resources/shopping-cart/css/style.css">
<script src="resources/js/jquery-2.1.1.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" ></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<title>Shopping | Bookworm</title>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
<head>

</head>
</c:if>
<body>
	<c:if test="${not empty param.logout}">
			<script type="text/javascript">toastr.success('Goodbye!', 'Successfully logged out', {timeout:2000});</script>
	</c:if>
	<div class="row">
		<div id="content" class="large-11s columns" style="padding-left: 40px">

			<jsp:include page="header.jsp" />
			<br>


		</div>
		

	</div>
	
	<!--  <h1 style="font-size: 260%">Need a good read? Try one of these!</h1> -->
	<div class="row">
		<body>
			<div class="row">


				<div class="large-12 columns">
					


<div class="row"><hr></div>
<div class="main">

	
	<a href="/Bookworm" style="padding-left:550px">Return To Home</a>
	<br>
	<hr>
	
	<h1 style="font-size: 200%">Browse Our Selection</h1>
	<h2 class="sub-heading" style="font-size: 120%">
		<i>Free Shipping Over $50!</i>
	</h2>

<section class="shopping-cart">
	<footer class="_grid cart-totals" style="padding-left: 100px">
		<div class="_column subtotal" id="subtotalCtr">
			<div class="cart-totals-key">Subtotal</div>
			<div class="cart-totals-value">$0.00</div>
		</div>
		<div class="_column shipping" id="shippingCtr">
			<div class="cart-totals-key">Shipping</div>
			<div class="cart-totals-value">$0.00</div>
		</div>
		<div class="_column taxes" id="taxesCtr">
			<div class="cart-totals-key">Taxes (6%)</div>
			<div class="cart-totals-value">$0.00</div>
		</div>
	
			<div class="_column total" id="totalCtr">
				<div class="caxrt-totals-key">Total</div>
				<div id="thetotal" class="cart-totals-value">$0.00</div>
			</div>

		<div class="_column checkout">
			<a href="checkout"><button class="_btn checkout-btn entypo-forward">Checkout</button></a>
		</div>
	</footer>
</section>


<section class="shopping-cart">
		<ol class="ui-list shopping-cart--list" id="shopping-cart--list">

<c:forEach items="${bookList}" var="book">
<div id="shopping-cart--list-item-template" type="text/template">
				
				<li class="_grid shopping-cart--list-item">
					<div class="_column product-image" style="background-image: url(&quot;${book.image_url}&quot;)">
					</div>
					<div class="_column product-info">
						<h4 class="product-name">${book.title}</h4>
						<p class="product-desc">${book.overview}</p>
						<div class="price product-single-price">${book.price}</div>
					</div>
					<div class="_column product-modifiers"
						data-product-price="${book.price}">
						<div class="_grid">
							<button class="_btn _column product-subtract">&minus;</button>
							<div class="_column product-qty">0</div>
							<button class="_btn _column product-plus">&plus;</button>
						</div>
						<button class="_btn entypo-trash product-remove">Remove</button>
						<div class="price product-total-price">$0.00</div>
					</div>
				</li>
					</div>	
</c:forEach>


		</ol>



	</section>
</div>


<center><a class="back-to-top" style="display: inline;" href="#">Back to Top</a></center>

<hr>

<footer>

		<div class="row">
			<div class="small-3 medium-1 columns">
				<p class="bottom">Contact Us:</p>
			</div>

			<div class="small-3 medium-2 columns">
				<p class="bottom">
					<a>Phone (555) 555-5555</a>
				</p>
			</div>

			<div class="small-3 medium-2 columns">
				<a href="mailto:helpdesk@bookworm.com?Subject=Customer Inquiry"
					target="_top" class="bottom">Send Email</a>
			</div>

			<div class="small-4 columns">
				<p class="bottom" align="right">&copy BookWorm 2016</p>
			</div>
		</div>
		<br>

	</footer>


<script
	src='http://cdnjs.cloudflare.com/ajax/libs/zepto/1.0/zepto.min.js'></script>

</div>


<script src="resources/js/jquery.mixitup.min.js"></script>
<script src="resources/js/main.js"></script>
<!-- Resource jQuery -->


<script src="resources/js/vendor/foundation.js"></script>
<!-- <script src="resources/shopping-cart/js/prefixfree.js"></script> -->
<script src="resources/shopping-cart/js/new.js"></script>
<!-- Other JS plugins can be included here -->

<script>
	$(document).foundation();
</script>
<!--</div>-->
</div>
</div>
<c:if test="${!ajaxRequest}">
	</body>
	</html>
</c:if>