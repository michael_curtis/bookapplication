<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="d"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<header style="padding-top:50px">
		<div class="large-1 columns" style="padding-right: 20px">
		<div id = "filters">
			<div class="cd-filter">
				<form action="">
					<div class="cd-filter-block">
						<h4>Search</h4>

						<div class="cd-filter-content">
							<input type="search" placeholder="Title, Author, ISBN#"
								style="font-color: #213963">

							<ul class="cd-filter-content cd-filters list">
								<li><input class="filter" data-filter=".radio1"
									type="radio" name="search" id="radio1" checked> <label
									class="radio-label" for="radio1">Title</label></li>

								<li><input class="filter" data-filter=".radio2"
									type="radio" name="search" id="radio2"> <label
									class="radio-label" for="radio2">Author</label></li>

								<li><input class="filter" data-filter=".radio3"
									type="radio" name="search" id="radio3"> <label
									class="radio-label" for="radio3">ISBN#</label></li>
							</ul>

						</div>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Book Type</h4>

						<ul class="cd-filter-content cd-filters list">
							<li><input class="filter" data-filter=".check1"
								type="checkbox" id="checkbox1"> <label
								class="checkbox-label" for="checkbox1">Print Book</label></li>

							<li><input class="filter" data-filter=".check2"
								type="checkbox" id="checkbox2"> <label
								class="checkbox-label" for="checkbox2">EBook</label></li>

						</ul>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Price Range</h4>

						<div class="cd-filter-content">
							<div class="cd-select cd-filters">
								<select class="filter" name="selectThis" id="selectThis">
									<option value="">Choose an option</option>
									<option value=".option1">$0 - $10</option>
									<option value=".option2">$10 - $50</option>
									<option value=".option3">$50 - $100</option>
									<option value=".option4">Over $100</option>
								</select>
							</div>
							<!-- cd-select -->
						</div>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Genre</h4>

						<ul class="cd-filter-content cd-filters list">
							<li><input class="filter" data-filter=".radio1" type="radio"
								name="genre" id="radio4" checked> <label
								class="radio-label" for="radio4">Search All</label></li>

							<li><input class="filter" data-filter=".radio1" type="radio"
								name="genre" id="radio5"> <label class="radio-label"
								for="radio5">Fiction</label></li>

							<li><input class="filter" data-filter=".radio2" type="radio"
								name="genre" id="radio6"> <label class="radio-label"
								for="radio6">Non-Fiction</label></li>

							<li><input class="filter" data-filter=".radio3" type="radio"
								name="genre" id="radio7"> <label class="radio-label"
								for="radio7">Teen</label></li>

							<li><input class="filter" data-filter=".radio4" type="radio"
								name="genre" id="radio8"> <label class="radio-label"
								for="radio8">Children</label></li>

							<li><input class="filter" data-filter=".radio5" type="radio"
								name="genre" id="radio9"> <label class="radio-label"
								for="radio9">Textbook/Study Help</label></li>

							<li><input class="filter" data-filter=".radio6" type="radio"
								name="genre" id="radio10"> <label class="radio-label"
								for="radio10">Sale</label></li>
						</ul>
						<!-- cd-filter-content -->
					</div>
					<!-- cd-filter-block -->
				</form>
				<a href="#0" class="cd-close">Close</a>
			</div>
	
			<!-- cd-filter -->

			<a href="#0" class="cd-filter-trigger">Refine Search</a>
		</div>
		</div>
	<div class="row" style="background-color: #ecf0f1">
		<div class="medium-4 columns">
			<a href="/Bookworm"> <img src="/Bookworm/resources/BookWormLogo/BookWormLogoLong.png" alt="company logo"> </a>
		</div>
		<div class="medium-5 columns">
			<div class="row" style="padding:20px 0px 0px 0px;">
				<form:form method = "POST" action = "/Bookworm/search" modelAttribute = "book">
				<div class="medium-9 columns">
					<form:input type="text" path = "title" placeholder="Title, Author, ISBN#"
						style="height:40px"/>
				</div>
				<div class="medium-3 columns">
					<input type="submit" value = "SEARCH" style="background-color: #213963"
											class="alert button expand" />
				</div>
				</form:form>
			</div>
		</div>
		<div class="medium-3 columns">
		
			<div class="row">
				<d:if test="${!empty user.firstName}">
					<div class="medium-5 small-4 columns" style="font-size: 13px">
						<p><br>Signed in as: <br>
						${user.firstName}</p>
					</div>
					<div class="medium-7 small-8 columns">
					<a href="/Bookworm/user/logout" style="color: #213963"><img style="float:left; padding-top:25px"
							role="menuitem" src="/Bookworm/resources/img/register.ico" height="20px"
							width="20px" alt="">
							<p style ="font-size:16px; padding-top:25px">Log Out</p>
							<img src="" alt=""></img></a>
					</div>
				</d:if>
				<d:if test="${empty user.firstName}">
					<div class="medium-6 small-4 columns">
					<a href="login" style="color: #213963"><img style="float:left; padding-top:30px"
							role="menuitem" src="/Bookworm/resources/img/System Login.ico"
							height="20px" width="20px" alt=""><p style="padding-top:25px">Login</p></a>
					</div>
					<div class="medium-6 columns small-8">
					<a href="signup" style="color: #213963"><img style="float:left; padding-top:30px"
							role="menuitem" src="/Bookworm/resources/img/register.ico" height="20px"
							width="20px" alt=""><p style="padding-top:25px">Register</p><img src="" alt=""></img></a>
					</div>
				</d:if>
			</div>					
		</div>
	</div>
</header>