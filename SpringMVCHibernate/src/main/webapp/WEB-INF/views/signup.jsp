<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>
<html>

<head>
    <link rel="stylesheet" href="resources/css/ourStyle.css">
    <link rel="stylesheet" href="resources/css/foundation.min.css">
    <link rel="stylesheet" href="resources/css/overwriteFoundationStyles.css">
    <title>Sign In | Bookworm</title>
</head>

<body class="bckImg">

<nav>
</nav>

<!--  -->
<!--<h2 class="text-center" style="color:rgb(230,230,230)">Create A <span style="color: #123456;">Bookworm</span> Account</h2>
-->
<div class="center-form">
	<form:form action="user/signup" method="POST" modelAttribute="user">
		     <div class="text-center" style="padding-bottom:10px;">
    <h4 class="text-center" style="color:#213963;" style="padding-bottom:10px;">Create A New <br>BookWorm Account</br></h4>
 		<a href="/Bookworm"><img src="resources/BookWormLogo/BookWormLogoCir3.png" style="width:128px; padding-bottom:10px; padding-top:10px;" alt="Book Picture " /></a>
 		</div>
		
		<c:if test="${not empty param.emailexists}">
			<p style="color:red"><c:out value = "A user with this email already exists." /></p>
		</c:if>
		
		<spring:message text="Email"/>
		<form:label path="email" style="color: #213963">
		</form:label>
		<form:input path="email" required="true" autofocus="true" type="email"/>
		
		<form:label path="firstName" style="color: #213963">
			<spring:message text="First Name"/>
		</form:label>
		<form:input path="firstName" required="true"/>

		<form:label path="lastName" style="color: #213963">
			<spring:message text="Last Name"/>
		</form:label>
		<form:input path="lastName" required="true"/>
		
		<form:label path="password" style="color: #213963">
			<spring:message text="Password"/>
		</form:label>
		<form:input path="password" type="password" required="true"/>
		
		<input type="submit" class="button medium float-center" style="background-color:#213963"  value="<spring:message text="Create My Account"/>" />
	</form:form>    


    <p style="font-size:medium" style="color: #213963">Already a user? <a href="login">Sign in now.</a></p>

</div>
</body>
</html>