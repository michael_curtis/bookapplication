/* jslint browser: true*/
/*global $*/

// https://github.com/jasonmoo/t.js
(function(){function c(a){this.t=a}function l(a,b){for(var e=b.split(".");e.length;){if(!(e[0]in a))return!1;a=a[e.shift()]}return a}function d(a,b){return a.replace(h,function(e,a,i,f,c,h,k,m){var f=l(b,f),j="",g;if(!f)return"!"==i?d(c,b):k?d(m,b):"";if(!i)return d(h,b);if("@"==i){e=b._key;a=b._val;for(g in f)f.hasOwnProperty(g)&&(b._key=g,b._val=f[g],j+=d(c,b));b._key=e;b._val=a;return j}}).replace(k,function(a,c,d){return(a=l(b,d))||0===a?"%"==c?(new Option(a)).innerHTML.replace(/"/g,"&quot;"):
a:""})}var h=/\{\{(([@!]?)(.+?))\}\}(([\s\S]+?)(\{\{:\1\}\}([\s\S]+?))?)\{\{\/\1\}\}/g,k=/\{\{([=%])(.+?)\}\}/g;c.prototype.render=function(a){return d(this.t,a)};window.t=c})();
// end of 't';

Number.prototype.to_$ = function () {
  return "$" + parseFloat( this ).toFixed(2);
};
String.prototype.strip$ = function () {
  return this.split("$")[1];
};

var app = {

  shipping : 5.00,
  /*products : [
              {
                  "name" : "George Washington's Secret Six",
                  "price" : "11.00",
                  "img" : "https://images-na.ssl-images-amazon.com/images/I/61wPkMdJJ3L._AC_US160_.jpg",
                  "desc" : "Paperback - October 28, 2014 by Brian Kileade"
              },
                {
                  "name" : "The Lost Island",
                  "price" : "6.50",
                  "img" : "https://images-na.ssl-images-amazon.com/images/I/51al4hjlonL._AC_US160_.jpg",
                  "desc" : "Paperback - March 24, 2015 by Douglas Preston"
                },
                {
                  "name" : "I Was Here",
                  "price" : "7.20",
                  "img" : "https://images-na.ssl-images-amazon.com/images/I/51JBIqZ0fwL._AC_US160_.jpg",
                  "desc" : "Paperback - January 26, 2016 by Gayle Forman"
                },
                {
                    "name" : "Jesus Calling",
                    "price" : "9.09",
                    "img" : "https://images-na.ssl-images-amazon.com/images/I/51MDhg8HALL._AC_US160_.jpg",
                    "desc" : "Hardcover - October 10, 2004 by Sarah Young"
                  },
                  {
                      "name" : "Seveneves",
                      "price" : "10.90",
                      "img" : "https://images-na.ssl-images-amazon.com/images/I/51J5y4nLPFL._AC_US160_.jpg",
                      "desc" : "Paperback - May 17, 2016 by Neal Stephenson"
                    },
                    {
                        "name" : "The Cellar",
                        "price" : "7.99",
                        "img" : "https://images-na.ssl-images-amazon.com/images/I/51bh627nh9L._AC_US160_.jpg",
                        "desc" : "Paperback - March 1, 2014 by Natasha Preston"
                      },
                      {
                          "name" : "The Alchemist",
                          "price" : "10.23",
                          "img" : "https://images-na.ssl-images-amazon.com/images/I/516c6gUQLaL._AC_US160_.jpg",
                          "desc" : "Paperback - April 15, 2014 by Paulo Coelho"
                        },
                        {
                            "name" : "The Witness of the Stars",
                            "price" : "13.95",
                            "img" : "https://images-na.ssl-images-amazon.com/images/I/41CPDgTcfdL._AC_US160_.jpg",
                            "desc" : "Paperback - August 22, 2016 by E. W. Bullinger"
                          },
                {
                  "name" : "The Harbinger",
                  "img" : "https://images-na.ssl-images-amazon.com/images/I/51vFZ4P7weL._AC_US160_.jpg",
                  "price" : "9.90",
                  "desc" : "Paperback - January 3, 2012 by Jonathan Cahn"
                }
    ]*/

  removeProduct: function () {
    "use strict";

    var item = $(this).closest(".shopping-cart--list-item");

    item.addClass("closing");
    window.setTimeout( function () {
      item.remove();
      app.updateTotals();
    }, 500); // Timeout for css animation
  },

  addProduct: function () {
    "use strict";

    var qtyCtr = $(this).prev(".product-qty"),
        quantity = parseInt(qtyCtr.html(), 10) + 1;

    app.updateProductSubtotal(this, quantity);
  },

  subtractProduct: function () {
    "use strict";

    var qtyCtr = $(this).next(".product-qty"),
        num = parseInt(qtyCtr.html(), 10) - 1,
        quantity = num <= 0 ? 0 : num;

    app.updateProductSubtotal(this, quantity);
  },

  updateProductSubtotal: function (context, quantity) {
    "use strict";

    var ctr = $(context).closest(".product-modifiers"),
        productQtyCtr = ctr.find(".product-qty"),
        productPrice = parseFloat(ctr.data("product-price")),
        subtotalCtr = ctr.find(".product-total-price"),
        subtotalPrice = quantity * productPrice;

    productQtyCtr.html(quantity);
    subtotalCtr.html( subtotalPrice.to_$() );

    app.updateTotals();
  },

  updateTotals: function () {
    "use strict";

    var products = $(".shopping-cart--list-item"),
        subtotal = 0,
        shipping;

    for (var i = 0; i < products.length; i += 1) {
      subtotal += parseFloat( $(products[i]).find(".product-total-price").html().strip$() );
    }

    shipping = (subtotal > 0 && subtotal < (50)) ? app.shipping : 0;

    $("#subtotalCtr").find(".cart-totals-value").html( subtotal.to_$() );
    $("#taxesCtr").find(".cart-totals-value").html( (subtotal * 0.06).to_$() );
    $("#totalCtr").find(".cart-totals-value").html( (subtotal * 1.06 + shipping).to_$() );
    $("#shippingCtr").find(".cart-totals-value").html( shipping.to_$() );
  },

  attachEvents: function () {
    "use strict";

    $(".product-remove").on("click", app.removeProduct);
    $(".product-plus").on("click", app.addProduct);
    $(".product-subtract").on("click", app.subtractProduct);
  },

//  setProductImages: function () {
//    "use strict";
//
//    var images = $(".product-image"),
//        ctr,
//        img;
//
//    for (var i = 0; i < images.length; i += 1) {
//      ctr = $(images[i]),
//      img = ctr.find(".product-image--img");
//
//      ctr.css("background-image", "url(" + img.attr("src") + ")");
//      img.remove();
//    }
//  },

//  renderTemplates: function () {
//    "use strict";
//
//    var products = app.products,
//        content = [],
//        template = new t( $("#shopping-cart--list-item-template").html() );
//
//    for (var i = 0; i < products.length; i += 1) {
//      content[i] = template.render(products[i]);
//    }
//
//    $("#shopping-cart--list").html(content.join(""));
//  }

};

app.attachEvents();